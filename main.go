package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"notesbot/packages/config"
	"notesbot/packages/storage"
	"notesbot/packages/telegram"

	"go.uber.org/fx"
)

func main() {
	app := fx.New(
		fx.Provide(
			config.Populate,
			telegram.NewBot,
			telegram.NewUpdateChannel,
			storage.NewClient,
			storage.NewStorage,
			telegram.StartHandler,
		),
		fx.Invoke(
			func(handler telegram.Handler) {
				go handler.Handle()
			},
		),
		fx.StartTimeout(10*time.Second),
		fx.StopTimeout(5*time.Second),
	)

	startCtx, cancel := context.WithTimeout(context.Background(), app.StartTimeout())
	defer cancel()

	err := app.Start(startCtx)

	if err != nil {
		panic(err)
	}

	log.Println("Application successfully started")
	sigs := app.Done()
	sig := <-sigs

	log.Println("\n Received signal: ", sig)
	log.Println("Exiting in " + app.StopTimeout().String())
	stopCtx, cancel := context.WithTimeout(context.Background(), app.StopTimeout())
	defer cancel()

	fmt.Println("Stopping application...")

	log.Fatal(app.Stop(stopCtx))
}
