module notesbot

go 1.17

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.4.0
	github.com/jinzhu/configor v1.2.1
	go.mongodb.org/mongo-driver v1.8.0
	go.uber.org/fx v1.16.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.0.2 // indirect
	github.com/xdg-go/stringprep v1.0.2 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.uber.org/atomic v1.6.0 // indirect
	go.uber.org/dig v1.12.0 // indirect
	go.uber.org/multierr v1.5.0 // indirect
	go.uber.org/zap v1.16.0 // indirect
	golang.org/x/crypto v0.0.0-20201216223049-8b5274cf687f // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210903071746-97244b99971b // indirect
	golang.org/x/text v0.3.5 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
