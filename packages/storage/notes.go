package storage

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Note struct {
	Id        string     `bson:"_id"     json:"id"`
	Headline  string     `bson:"headline" json:"headline"`
	Text      string     `bson:"text"    json:"text"`
	OwnerId   int64      `bson:"ownerId" json:"ownerId"`
	CreatedAt *time.Time `bson:"createdAt" json:"createdAt"`
	UpdatedAt *time.Time `bson:"updatedAt" json:"updatedAt"`
}

func (s *storage) AddHeadline(name string, id int, ownerId int64) error {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "_id", Value: id}, primitive.E{Key: "ownerId", Value: ownerId}}
	update := bson.D{
		primitive.E{
			Key: "$set", Value: bson.D{
				primitive.E{Key: "headline", Value: name},
				primitive.E{Key: "updatedAt", Value: time.Now()},
			},
		},
		primitive.E{
			Key: "$setOnInsert", Value: bson.D{primitive.E{Key: "createdAt", Value: time.Now()}},
		},
	}
	options := options.FindOneAndUpdate().SetUpsert(true)

	res := collection.FindOneAndUpdate(ctx, filter, update, options)
	return res.Err()
}

func (s *storage) AddNote(text string, id int, ownerId int64) error {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "_id", Value: id}, primitive.E{Key: "ownerId", Value: ownerId}}
	update := bson.D{
		primitive.E{
			Key: "$set", Value: bson.D{
				primitive.E{Key: "text", Value: text},
				primitive.E{Key: "updatedAt", Value: time.Now()},
			},
		},
		primitive.E{
			Key: "$setOnInsert", Value: bson.D{primitive.E{Key: "createdAt", Value: time.Now()}},
		},
	}
	options := options.FindOneAndUpdate().SetUpsert(true)

	res := collection.FindOneAndUpdate(ctx, filter, update, options)
	return res.Err()
}

func (s *storage) RemoveNote(id int, ownerId int64) error {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "_id", Value: id}, primitive.E{Key: "ownerId", Value: ownerId}}
	_, err := collection.DeleteOne(ctx, filter)

	return err
}

func (s *storage) GetNote(id int, ownerId int64) (Note, error) {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{
		primitive.E{Key: "_id", Value: id},
		primitive.E{Key: "ownerId", Value: ownerId},
	}

	res := collection.FindOne(ctx, filter)
	note := Note{}

	if res.Err() != nil && res.Err() != mongo.ErrNoDocuments {
		return note, res.Err()
	} else if res.Err() == mongo.ErrNoDocuments {
		return note, nil
	}

	err := res.Decode(&note)

	return note, err
}

func (s *storage) ListNotes(ownerId int64) ([]Note, error) {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "ownerId", Value: ownerId}}
	count, err := collection.CountDocuments(ctx, filter)

	if err != nil {
		return []Note{}, err
	}

	options := options.Find().SetSort(bson.D{primitive.E{Key: "createdAt", Value: -1}})

	data, err := collection.Find(ctx, filter, options)

	if err != nil {
		return []Note{}, err
	}
	notes := make([]Note, count)

	err = data.All(context.Background(), &notes)
	if err != nil {
		return []Note{}, err
	}

	return notes, nil
}
