package storage

import (
	"notesbot/packages/config"

	"go.mongodb.org/mongo-driver/mongo"
)

func NewStorage(
	client *mongo.Client,
	config config.Config,
) Storage {
	return &storage{
		client: client,
		config: config,
	}
}

type Storage interface {
	AddHeadline(name string, id int, ownerId int64) error
	AddNote(text string, id int, ownerId int64) error
	RemoveNote(id int, ownerId int64) error
	GetNote(id int, ownerId int64) (Note, error)
	ListNotes(ownerId int64) ([]Note, error)

	AddUser(id int64) (User, error)

	GetStatus(ownerId int64) (Status, error)
	UpdateStatus(ownerId int64, newStatus WorkStatus, activeNoteId int) (Status, error)
}

type storage struct {
	client *mongo.Client
	config config.Config
}
