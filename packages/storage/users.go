package storage

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	Id        int64      `bson:"_id"     json:"id"`
	CreatedAt *time.Time `bson:"createdAt" json:"createdAt"`
	UpdatedAt *time.Time `bson:"updatedAt" json:"updatedAt"`
}

func (s *storage) AddUser(id int64) (User, error) {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.UsersCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "_id", Value: id}}
	update := bson.D{
		primitive.E{
			Key: "$set", Value: bson.D{
				primitive.E{Key: "status", Value: STATUS_INITIAL},
				primitive.E{Key: "updatedAt", Value: time.Now()},
			},
		},
		primitive.E{
			Key: "$setOnInsert", Value: bson.D{primitive.E{Key: "createdAt", Value: time.Now()}},
		},
	}
	options := options.FindOneAndUpdate().SetUpsert(true).SetReturnDocument(options.After)

	res := collection.FindOneAndUpdate(ctx, filter, update, options)
	user := User{}

	if res.Err() != nil {
		return user, nil
	}

	if err := res.Decode(user); err != nil {
		return user, nil
	}

	return user, nil
}
