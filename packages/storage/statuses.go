package storage

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type WorkStatus string

const (
	STATUS_INITIAL  WorkStatus = "initial"
	STATUS_HEADLINE WorkStatus = "headline"
	STATUS_WRITING  WorkStatus = "writing"
)

type Status struct {
	OwnerId      int64      `bson:"_id" json:"ownerId"`
	Status       WorkStatus `bson:"status"  json:"status"`
	ActiveNoteId int        `bson:"activeNoteId" json:"activeNoteId"`
}

func (s *storage) GetStatus(ownerId int64) (Status, error) {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "_id", Value: ownerId}}
	update := bson.D{primitive.E{Key: "$setOnInsert", Value: bson.D{
		primitive.E{Key: "status", Value: STATUS_INITIAL},
		primitive.E{Key: "activeNoteId", Value: 0},
	}}}
	options := options.FindOneAndUpdate().SetUpsert(true).SetReturnDocument(options.After)

	res := collection.FindOneAndUpdate(ctx, filter, update, options)

	status := Status{}

	if res.Err() != nil {
		return status, res.Err()
	}

	if err := res.Decode(status); err != nil {
		return status, nil
	}

	return status, nil
}

func (s *storage) UpdateStatus(ownerId int64, newStatus WorkStatus, activeNoteId int) (Status, error) {
	db := s.client.Database(s.config.Storage.Database)
	collection := db.Collection(s.config.Storage.NotesCollection)
	ctx, cancel := context.WithTimeout(context.Background(), s.config.Storage.WriteTimeoutDuration)
	defer cancel()

	filter := bson.D{primitive.E{Key: "_id", Value: ownerId}}
	update := bson.D{
		primitive.E{
			Key: "$set", Value: bson.D{
				primitive.E{Key: "status", Value: string(newStatus)},
				primitive.E{Key: "activeNoteId", Value: activeNoteId},
			},
		},
	}
	options := options.FindOneAndUpdate().SetUpsert(true).SetReturnDocument(options.After)

	res := collection.FindOneAndUpdate(ctx, filter, update, options)

	status := Status{}

	if res.Err() != nil {
		return status, nil
	}

	if err := res.Decode(status); err != nil {
		return status, nil
	}

	return status, nil
}
