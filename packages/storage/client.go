package storage

import (
	"context"
	"fmt"
	"notesbot/packages/config"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/fx"
)

func NewClient(
	config config.Config,
	lc fx.Lifecycle,
) (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	fmt.Println("Connection url", config.Storage.Url)
	clientOptions := options.Client().ApplyURI(config.Storage.Url)
	client, err := mongo.Connect(ctx, clientOptions)

	lc.Append(fx.Hook{
		OnStart: func(c context.Context) error {
			return client.Ping(c, nil)
		},
		OnStop: func(c context.Context) error {
			return client.Disconnect(c)
		},
	})

	return client, err
}
