package telegram

import (
	"fmt"
	"log"
	"notesbot/packages/storage"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/pkg/errors"
	"go.uber.org/fx"
)

type Command string

const (
	COMMAND_CREATE_NOTE Command = "/note"
	COMMAND_LIST_NOTES  Command = "/list"
)

func (c Command) String() string {
	return string(c)
}

func StartHandler(
	lc fx.Lifecycle,
	bot *tgbotapi.BotAPI,
	channel tgbotapi.UpdatesChannel,
	storage storage.Storage,
) Handler {
	return &handler{
		bot:     bot,
		channel: channel,
		storage: storage,
	}
}

type Handler interface {
	Handle()
}

type handler struct {
	bot     *tgbotapi.BotAPI
	channel tgbotapi.UpdatesChannel
	storage storage.Storage
}

func (h *handler) Handle() {
	for update := range h.channel {
		if update.Message != nil {
			_, _ = h.storage.AddUser(update.Message.From.ID)
			status, _ := h.CheckStatus(update.Message.From.ID)
			response := "empty"

			if update.Message.Text == COMMAND_CREATE_NOTE.String() || status.Status == storage.STATUS_WRITING {
				response = h.HandleNote(status, update.Message.Text, update.Message.MessageID)
			}

			if update.Message.Text == COMMAND_LIST_NOTES.String() {
				response = h.HandleList(status)
			}

			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

			msg := tgbotapi.NewMessage(update.Message.Chat.ID, response)
			msg.ReplyToMessageID = update.Message.MessageID

			_, err := h.bot.Send(msg)
			if err != nil {
				log.Printf("Something went wrong %s", err.Error())
			}
		}
	}
}

func (h *handler) CheckStatus(userId int64) (storage.Status, error) {
	return h.storage.GetStatus(userId)
}

func (h *handler) HandleNote(status storage.Status, text string, messageId int) string {
	if text == COMMAND_CREATE_NOTE.String() {
		_ = h.storage.RemoveNote(status.ActiveNoteId, status.OwnerId)
		_, _ = h.storage.UpdateStatus(status.OwnerId, storage.STATUS_HEADLINE, messageId)
		return "choose headline"
	}

	if status.Status == storage.STATUS_HEADLINE {
		_ = h.storage.AddHeadline(text, status.ActiveNoteId, status.OwnerId)
		_, _ = h.storage.UpdateStatus(status.OwnerId, storage.STATUS_WRITING, status.ActiveNoteId)
		return "now write text"
	}

	if status.Status == storage.STATUS_WRITING {
		_ = h.storage.AddNote(text, status.ActiveNoteId, status.OwnerId)
		_, _ = h.storage.UpdateStatus(status.OwnerId, storage.STATUS_INITIAL, status.ActiveNoteId)
		return "successfully added"
	}

	return "noop"
}

func (h *handler) HandleList(status storage.Status) string {
	notes, err := h.storage.ListNotes(status.OwnerId)

	if err != nil {
		return errors.Wrap(err, "Oops").Error()
	}

	ret := ""
	for i, note := range notes {
		ret += fmt.Sprintf("%d. %s\n", i, note.Headline)
	}

	return ret
}
