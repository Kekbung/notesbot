package config

import (
	"time"

	"github.com/jinzhu/configor"
	"github.com/pkg/errors"
)

type Config struct {
	Bot     Bot
	Storage Storage
}

func (c Config) parse() (Config, error) {
	var err error
	c.Storage, err = c.Storage.parse()

	return c, err
}

type Storage struct {
	Url                    string `required:"true"`
	Database               string `required:"true"`
	UsersCollection        string `yaml:"usersCollection" required:"true"`
	NotesCollection        string `yaml:"notesCollection" required:"true"`
	StatusesCollection     string `yaml:"statusesCollection" required:"true"`
	ConnectTimeout         string `yaml:"connectTimeout" required:"true"`
	WriteTimeout           string `yaml:"writeTimeout" required:"true"`
	ConnectTimeoutDuration time.Duration
	WriteTimeoutDuration   time.Duration
}

func (s Storage) parse() (Storage, error) {
	var err error
	s.ConnectTimeoutDuration, err = time.ParseDuration(s.ConnectTimeout)
	if err != nil {
		panic(errors.Wrap(err, "Couldn't read connect timeout duration"))
	}

	s.WriteTimeoutDuration, err = time.ParseDuration(s.WriteTimeout)
	if err != nil {
		panic(errors.Wrap(err, "Couldn't read write timeout duration"))
	}

	return s, err
}

type Bot struct {
	Token         string `env:"botToken" required:"true"`
	UpdateTimeout int    `yaml:"updateTimeout"`
	Debug         bool
}

func Populate() (Config, error) {
	conf := Config{}

	configor.New(&configor.Config{
		ErrorOnUnmatchedKeys: true,
		Verbose:              true,
	}).Load(&conf, "config/config.yaml")

	return conf.parse()
}
